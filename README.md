# CucumberSeleniumFramework

<b>Language:</b> <br>
  This Selenium WebDriver project is implemented using Java as a language binding

<b>Type of Framework:</b> <br>Data-driven framework using POM as design pattern with BDD approach

<b>Packages:</b> <br>
	1) .base - contains browser specific configuration <br>
	2) .helper - contains classes for commonly used methods & configuration <br>
	3) .page - Page objects <br>
	4) stepdefinitions - contains step definitions <br>
	5) testrunner - contains required configuration to run the test pack <br>
  
<b>Folder:</b> <br>
	1) Features - contains feature files aka test cases for acceptance criteria <br>
	2) Configuration - contains config files <br>
	3) Drivers - this include browser's driver <br>
	4) Report - this folder will generate the extent report once the execution is complete <br>

<b>How to Run ? </b><br>
There are two ways to run the test suite. <br>
	1) Go to TestRun.java to run the test suite as JUnit <br>
	2) Individual feature files can also be executed by running it as cucumber feature on right-click <br>

<b><u>GIF: </b><br>
<br>	
![phptravels_demo](https://user-images.githubusercontent.com/58862186/71799189-9f997880-307a-11ea-8534-ae7326bf64a2.gif)
